include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})

add_executable(runServer_Tests main_test.cpp)

target_link_libraries(runServer_Tests gtest gtest_main)
#target_link_libraries(runServer_Tests gtest_demo)