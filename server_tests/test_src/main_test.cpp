//
// Created by John Arvin Aboque on 9/29/16.
//

#include "main_test.h"
#include "gtest/gtest.h"

TEST(sample_test, test_equality){
    EXPECT_EQ(0, 0);
}

int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}